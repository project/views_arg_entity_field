<?php

namespace Drupal\Tests\views_arg_entity_field\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests the main plugin.
 *
 * @group views_arg_entity_field
 */
class ViewsArgEntityFieldTest extends BrowserTestBase {

  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['views_arg_entity_field_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createTestNodes();
  }

  /**
   * Tests the frontpage.
   */
  public function testViewsArgEntityField(): void {
    $assert_session = $this->assertSession();

    // Entity reference field where target_id is main property.
    $this->updateDefaultArgumentOptions('field_name', 'field_reference:target_id');
    $this->drupalGet('node/1');
    $assert_session->pageTextNotContains('Node 2');
    $assert_session->pageTextContains('Node 3');
    $assert_session->pageTextNotContains('Node 4');
    $this->drupalGet('node/2');
    $assert_session->pageTextContains('Node 1');
    $assert_session->pageTextContains('Node 3');
    $assert_session->pageTextNotContains('Node 4');

    // Field with "value" as main property.
    $this->updateDefaultArgumentOptions('field_name', 'field_integer:value');
    $this->drupalGet('node/1');
    $assert_session->pageTextContains('Node 2');
    $assert_session->pageTextNotContains('Node 3');
    $assert_session->pageTextNotContains('Node 4');
    $this->drupalGet('node/2');
    $assert_session->pageTextContains('Node 1');
    $assert_session->pageTextContains('Node 3');
    $assert_session->pageTextNotContains('Node 4');

    // Field value that is not the main property (which would be uri for link).
    $this->updateDefaultArgumentOptions('field_name', 'field_link:title');
    $this->drupalGet('node/1');
    $assert_session->pageTextNotContains('Node 2');
    $assert_session->pageTextNotContains('Node 3');
    $assert_session->pageTextContains('Node 4');
    $this->drupalGet('node/2');
    $assert_session->pageTextContains('Node 1');
    $assert_session->pageTextContains('Node 3');
    $assert_session->pageTextNotContains('Node 4');

    // Single value, delta 0.
    $this->updateDefaultArgumentOptions('multiple_values', 'single');
    $this->drupalGet('node/1');
    $assert_session->pageTextNotContains('Node 2');
    $assert_session->pageTextNotContains('Node 3');
    $assert_session->pageTextContains('Node 4');
    $this->drupalGet('node/2');
    $assert_session->pageTextContains('Node 1');
    $assert_session->pageTextNotContains('Node 3');
    $assert_session->pageTextNotContains('Node 4');

    // Single Value, delta 1.
    $this->updateDefaultArgumentOptions('single_value_delta', '1');
    $this->drupalGet('node/1');
    $assert_session->pageTextNotContains('Node 2');
    $assert_session->pageTextNotContains('Node 3');
    $assert_session->pageTextNotContains('Node 4');
    $this->drupalGet('node/2');
    $assert_session->pageTextNotContains('Node 1');
    $assert_session->pageTextContains('Node 3');
    $assert_session->pageTextNotContains('Node 4');
  }

  /**
   * Helper function to update the settings on the default_argument plugin.
   */
  protected function updateDefaultArgumentOptions(string $key, string $value): void {
    $view = \Drupal::service('entity_type.manager')->getStorage('view')->load('arg_test');
    if ($view) {
      $executable = $view->getExecutable();
      $argument = $executable->getHandler('default', 'argument', 'nid');
      $argument['default_argument_options'][$key] = $value;
      $executable->setHandler('default', 'argument', 'nid', $argument);
      $executable->save();
    }
  }

  /**
   * Helper function to create nodes to test with.
   */
  protected function createTestNodes(): void {
    $this->createNode(
      [
        'type' => 'arg_test',
        'title' => 'Node 1',
        'field_integer' => 2,
        'field_reference' => '3',
        'field_link' => ['uri' => 'internal:/', 'title' => '4'],
      ]
    );
    $this->createNode(
      [
        'type' => 'arg_test',
        'title' => 'Node 2',
        'field_integer' => [1, 3],
        'field_reference' => [1, 3],
        'field_link' => [
          ['uri' => 'internal:/', 'title' => '1'],
          ['uri' => 'internal:/', 'title' => '3'],
        ],
      ]
    );

    $this->createNode(
      [
        'type' => 'arg_test',
        'title' => 'Node 3',
      ]
    );

    $this->createNode(
      [
        'type' => 'arg_test',
        'title' => 'Node 4',
      ]
    );

  }

}
